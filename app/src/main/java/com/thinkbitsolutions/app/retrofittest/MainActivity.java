package com.thinkbitsolutions.app.retrofittest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.thinkbitsolutions.app.retrofittest.models.Pokemon.Pokemon;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Build.ID;

public class MainActivity extends AppCompatActivity implements TextWatcher {


    EditText editTextPokeSearch;
    TextView textViewPokeInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextPokeSearch = (EditText) findViewById(R.id.edit_text_poke_search);
        editTextPokeSearch.addTextChangedListener(this);

        textViewPokeInfo = (TextView) findViewById(R.id.text_view_pokemon_info);

    }

    private void getPokemonDetailForId(int id) {


        RFTApp.pokeAPIService.getPokemonForId(id).enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {


                try {
                    final Pokemon pokemon = response.body();

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            textViewPokeInfo.setText(printPokemonInfoFor(pokemon));
                        }
                    });
                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {

            }
        });

    }

    private String printPokemonInfoFor(Pokemon pokemon) {


        return String.format(" POKEMON ID: %d \n" +
                " POKEMON NAME: %s \n POKEMON HEIGHT: %d \n POKEMON WEIGHT %d",
                pokemon.getId(), pokemon.getName(), pokemon.getHeight(), pokemon.getWeight());
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        try {
            getPokemonDetailForId(Integer.parseInt(charSequence.toString()));
        } catch (Exception e) {

        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
