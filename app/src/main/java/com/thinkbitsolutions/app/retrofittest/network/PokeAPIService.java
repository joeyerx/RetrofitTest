package com.thinkbitsolutions.app.retrofittest.network;

import android.support.annotation.Nullable;

import com.thinkbitsolutions.app.retrofittest.models.Pokemon.Pokemon;
import com.thinkbitsolutions.app.retrofittest.models.PokemonList.PokemonList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by joeyramirez on 10/30/2016.
 */

public interface PokeAPIService {



    @GET("pokemon/{id}/")//baseurl/pokemon/id
    Call<Pokemon> getPokemonForId(@Path("id")int id);

    @GET("pokemon")//baseurl/pokemon?limit=limit
    Call<PokemonList> getPokemonList(@Query("limit") int limit);

}
