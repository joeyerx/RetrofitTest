package com.thinkbitsolutions.app.retrofittest;

import android.app.Application;
import android.preference.PreferenceManager;

import com.thinkbitsolutions.app.retrofittest.network.PokeAPIService;
import com.thinkbitsolutions.app.retrofittest.network.PokeAPIServiceGenerator;

/**
 * Created by joeyramirez on 10/30/2016.
 */

public class RFTApp extends Application {


    public static PokeAPIService pokeAPIService;

    @Override
    public void onCreate() {
        super.onCreate();

        pokeAPIService = PokeAPIServiceGenerator.createService(PokeAPIService.class);
    }
}
